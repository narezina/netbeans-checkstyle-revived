# Netbeans Checkstyle Revived - Checkstyle plugin for Apache Netbeans

This is updated fork of netbeans-checkstyle plugin available from https://sickboy.cz

Old plugin was using obsolete packages, and was not able to install on latest Netbeans, thus this fork was made with intent to update old plugin.
